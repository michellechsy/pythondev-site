# Contributing to the Python dev slack community website

:+1::tada: First off, thanks for taking the time to contribute! :tada:

Contributions are welcome by anybody and everybody. We are not kidding! 

The rest of this document will be guidelines to contributing to the project. Remember that these are just guidelines, not rules. Use common sense as much as possible.


#### Table of Contents

- [Pull Request Guidelines](#pull-request-guidelines)
- [Types of Contributions](#types-of-contributions)
  - [Report Bugs](#report-bugs)
  - [Fix Bugs](#fix-bugs)
  - [Implement Features](#implement-features)
  - [Write Documentation](#write-documentation)
  - [Submit Feedback](#submit-feedback)
- [Get Started](#get-started)
- [Git Hygiene](#git-hygiene)
  - [Squash Commits](#squash-commits)
  - [Handling Merge Conflicts](#handling-merge-conflicts)    


## Pull Request Guidelines

Before you submit a pull request, check that it meets these guidelines:

1. The pull request should include tests (if necessary). If you have any questions about how to write tests then ask the community.
2. If the pull request adds functionality update the docs where appropriate.
3. [Use a good commit message](https://github.com/spring-projects/spring-framework/blob/30bce7/CONTRIBUTING.md#format-commit-messages)
4. [Squash your commits](#squash-commits)

If you have any issues with [Git Hygiene](#git-hygiene) might help.

## Types of Contributions

### Report Bugs

https://gitlab.com/PythonDevCommunity/pythondev-site

If you are reporting a bug, please include:

* Your operating system name and version.
* Any details about your local setup that might be helpful in troubleshooting.
* Detailed steps to reproduce the bug.

### Fix Bugs

Look through the gitlab issues for bugs. Anything tagged with "bug"
and "help wanted" is open to whoever wants to implement it.

A tag representing the difficulty might be present on the issues. Please let the issues
be resolved by people with the corresponding skills as this is a learning project.

### Implement Features

Look through the gitlab issues for features. Anything tagged with "enhancement"
and "help wanted" is open to whoever wants to implement it.

A tag representing the difficulty might be present on the issues. Please let the issues
be resolved by people with the corresponding skills as this is a learning project.

### Write Documentation

The pythondev-site could always use more documentation, whether as part of the
official pythondev-site docs, in docstrings, or even on the web in blog posts,
articles, and such.

### Submit Feedback

The best way to send feedback is to file an issue at https://gitlab.com/PythonDevCommunity/pythondev-site

If you are proposing a feature:

* Explain in detail how it would work.
* Keep the scope as narrow as possible, to make it easier to implement.
* Remember that this is a volunteer-driven project, and that contributions
  are welcome :)

## Get Started!

Ready to contribute? Here's how to set up `sir-bot-a-lot` for local development.

1. Fork the `pythondev-site` repo on gitlab.
2. Clone your fork locally:

    $ git clone git@gitlab.com:your_name_here/sir-bot-a-lot.git

3. Install your local copy into a virtualenv. Assuming you have virtualenvwrapper installed, this is how you set up your fork for local development:

    $ mkvirtualenv pythondev-site
    $ cd pythondev-site/
    $ python setup.py develop

4. Create a branch for local development::

    $ git checkout -b name-of-your-bugfix-or-feature

   Now you can make your changes locally.

5. When you're done making changes, check that your changes pass flake8 and the tests, including testing other Python versions with tox::

    $ python setup.py flake8
    $ python setup.py test or py.test
    $ tox

   To get flake8 and tox, just pip install them into your virtualenv.

6. Commit your changes and push your branch to gitlab::

    $ git add .
    $ git commit -m "Your detailed description of your changes."
    $ git push origin name-of-your-bugfix-or-feature

7. Submit a pull request through the gitlab website.

## Git Hygiene

### Squash Commits

 As a general rule of thumb, if a commit modifies a previous commit in the same PR, it probably needs to be squashed. That means that a PR may often only be a single commit. This makes rebasing (see below) easier, and keeps the history clean, which can make debugging infinitely easier in the long run. We also don't need records of only fixing whitespace or spelling mistakes in your PR.

- It's fine to make as many commits as you need while you're working on your local branch. Keeping your history clean as you work will probably be much easier than trying to do it all at the end, though.

- If you just want to make a change and have it apply to your last commit, you can use `git commit --amend`. If you want a change to be associated with an older commit, you can use `git commit -i HEAD~3` (where `3` is the number of commits to rebase). You can also use `git log` to find a commit's hash and `git rebase -i <commit hash>` (the commit should be the one PRIOR to the commit you want to modify).

- Interactive rebase (`git rebase -i`) will open your default editor in which you can replace `pick` with `fixup` or `f` to combine commits (you can also use this to reorder commits, mark commits to edit their commit messages, and other powerful tools which are explained in the file itself). Save the changes, and git will execute the rebase.

After rebasing, if your branch is already pushed up to GitHub, you'll have to force push the changes using `git push -f`, since the history has changed.

Do you have any questions ?

Feel free to ask them on the community-projects channel

**Warning:** Only rebase your own branches.

**Warning:** **DO NOT REBASE HOTFIXES** Otherwise :bomb: :sparkles:

### Handling Merge Conflicts

Occasionally a Pull Request will have Merge Conflicts. **Do not merge master into your branch.** Instead, make sure your `master` branch is up to date:

```sh
git checkout master
git pull
```

Then rebase your branch on `master`:

```sh
git checkout _my-branch_
git rebase master
```

If there are any conflicts you need to resolve, it will suspend the rebase for you to fix them. Then do:

```sh
git add .
git rebase --continue
```

It will do one round of conflict-checking for each commit in your branch, so keeping your history clean will make rebasing much easier. When the rebase is done, your branch will be up to date with master and ready to issue a PR if you are.